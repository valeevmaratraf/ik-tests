using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatorController : MonoBehaviour
{
    private Character character;
    private Animator animator;

    private void Start()
    {
        character = GetComponentInParent<Character>();
        animator = GetComponent<Animator>();

        character.StateChanged += HandleCharacterStateChange;
    }

    private void HandleCharacterStateChange(CharacterState oldState, CharacterState newState)
    {
        switch (newState)
        {
            case CharacterState.Move: animator.SetBool("IsMoving", true); break;
            case CharacterState.Idle: animator.SetBool("IsMoving", false); break;
        }
    }
}
