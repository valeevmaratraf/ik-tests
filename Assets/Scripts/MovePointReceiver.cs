using System;
using UnityEngine;

public class MovePointReceiver : MonoBehaviour
{
    public event Action<Vector3> NewMovePointRecieved;

    private void Update()
    {
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray, out var hitInfo))
        {
            return;
        }

        if (hitInfo.collider.gameObject.name == "Ground")
        {
            NewMovePointRecieved?.Invoke(hitInfo.point);
        }
    }
}
