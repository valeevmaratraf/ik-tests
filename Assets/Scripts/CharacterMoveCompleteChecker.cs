using System;
using UnityEngine;
using UnityEngine.AI;

public class CharacterMoveCompleteChecker : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private Animator animator;

    public event Action MovedToDestinationPoint;

    private bool isCompleteMoveEventDeclared;

    public Vector3 DestinationPoint
    {
        get => navMeshAgent.destination;
        set
        {
            if (navMeshAgent.destination == value)
            {
                return;
            }
            animator.speed = 1;
            isCompleteMoveEventDeclared = false;
            navMeshAgent.destination = value;
        }
    }

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        isCompleteMoveEventDeclared = true;
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (navMeshAgent.remainingDistance <= 0.01F && !isCompleteMoveEventDeclared)
        {
            isCompleteMoveEventDeclared = true;
            MovedToDestinationPoint?.Invoke();
        }
    }
}
