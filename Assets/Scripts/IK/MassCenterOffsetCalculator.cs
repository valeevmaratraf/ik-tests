using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassCenterOffsetCalculator : MonoBehaviour
{
    [SerializeField] private Transform massCenter;

    private FootholdCenterCalculatorIK resultFootholdCalculator;

    //private bool isInitialized;

    public Vector3 MassCenter => massCenter.position;
    public float MassCenterOffsetSqr
    {
        get
        {
            //if (!isInitialized)
            //{
            //    Initialize();
            //}

            return Vector3.SqrMagnitude(MassCenter - resultFootholdCalculator.transform.position);
        }
    }

    private void Start()
    {
        resultFootholdCalculator = GetComponent<FootholdCenterCalculatorIK>();
        //Initialize();
    }

    //private void Initialize()
    //{
    //    resultFootholdCalculator = GetComponent<FootholdCenterCalculatorIK>();
    //    isInitialized = true;
    //}

    private void Update()
    {
        //if (!isInitialized)
        //{
        //    Initialize();
        //}

        var projectedFootholdPost = resultFootholdCalculator.transform.position;
        projectedFootholdPost.y = massCenter.position.y;
        Debug.DrawLine(massCenter.transform.position, projectedFootholdPost, Color.red, Time.deltaTime);
    }
}
