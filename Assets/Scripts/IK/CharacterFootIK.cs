using Cysharp.Threading.Tasks;
using System.Collections;
using UnityEngine;

public class CharacterFootIK : MonoBehaviour
{
    [SerializeField] private AnimationCurve heightMoveCurve;
    [SerializeField] private float timeForStep;
    [SerializeField] private float stepHeight;

    [SerializeField] private Transform endBone;

    private Vector3 currentFootTargetPosition;
    private Coroutine currentStepProcess;

    public bool IsStepping => currentStepProcess != null;

    private void Start()
    {
        transform.position = endBone.transform.position;
        currentFootTargetPosition = transform.position;
    }

    private void Update()
    {
        Debug.DrawLine(endBone.position, currentFootTargetPosition, Color.green, Time.deltaTime);

        if (!IsStepping)
        {
            transform.position = currentFootTargetPosition;
        }
    }

    public void DoStep(Vector3 position)
    {
        currentFootTargetPosition = position;
        currentStepProcess = StartCoroutine(StepProcess());
    }

    private IEnumerator StepProcess()
    {
        Vector3 origin = transform.position;
        float timer = 0;

        while (timer < timeForStep)
        {
            float stepProgress = timer / timeForStep;
            var stepPosition = Vector3.Lerp(origin, currentFootTargetPosition, stepProgress);
            stepPosition += Vector3.up * heightMoveCurve.Evaluate(timer / timeForStep) * stepHeight;

            transform.position = stepPosition;
            timer += Time.deltaTime;

            yield return UniTask.WaitForEndOfFrame(this);
        }

        currentStepProcess = null;
    }
}
