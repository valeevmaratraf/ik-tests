using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepsController : MonoBehaviour
{
    [SerializeField] private FootholdCenterCalculatorIK leftLegFoothold;
    [SerializeField] private FootholdCenterCalculatorIK rightLegFoothold;

    [SerializeField] private MassCenterOffsetCalculator massCenterOffsetCalculator;
    [SerializeField] private FootholdCenterCalculatorIK resultFootholdCalculator;

    [SerializeField] private CharacterFootIK leftLeg;
    [SerializeField] private CharacterFootIK rightLeg;

    [SerializeField] private float doStepOffset;

    private void Update()
    {
        if (massCenterOffsetCalculator.MassCenterOffsetSqr < doStepOffset)
        {
            return;
        }

        bool leftLegOnGround = leftLegFoothold.IsOnGround();
        bool rightLegOnGround = rightLegFoothold.IsOnGround();

        if (!leftLegOnGround || !rightLegOnGround || leftLeg.IsStepping || rightLeg.IsStepping)
        {
            return;
        }

        Vector3 massCenter = massCenterOffsetCalculator.MassCenter;

        Vector3 leftLegProjectedFulcrum = leftLegFoothold.transform.position;
        leftLegProjectedFulcrum.y = massCenter.y;

        Vector3 rightLegProjectedFulcrum = rightLegFoothold.transform.position;
        rightLegProjectedFulcrum.y = massCenter.y;

        float leftLegDistanceToMassCenter = Vector3.SqrMagnitude(leftLegProjectedFulcrum - massCenter);
        float rightLegDistanceToMassCenter = Vector3.SqrMagnitude(rightLegProjectedFulcrum - massCenter);

        if (leftLegDistanceToMassCenter > rightLegDistanceToMassCenter)
        {
            Vector3 leftLegCompensateDelta = 2 * (massCenterOffsetCalculator.MassCenter - rightLegProjectedFulcrum);
            rightLegProjectedFulcrum.y = leftLeg.transform.position.y;
            leftLeg.DoStep(rightLegProjectedFulcrum + leftLegCompensateDelta);
        }
        else
        {
            Vector3 rightLegCompensateDelta = 2 * (massCenterOffsetCalculator.MassCenter - leftLegProjectedFulcrum);
            leftLegProjectedFulcrum.y = rightLeg.transform.position.y;
            rightLeg.DoStep(leftLegProjectedFulcrum + rightLegCompensateDelta);
        }
    }
}
