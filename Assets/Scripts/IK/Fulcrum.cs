using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fulcrum : MonoBehaviour
{
    [SerializeField] private Transform bone;
    [SerializeField] private float boneWidth;

    public float BoneWidth => boneWidth;

    public Vector3? GroundPoint
    {
        get
        {
            Ray ray = new Ray()
            {
                direction = Vector3.down,
                origin = bone.position
            };

            var hits = Physics.RaycastAll(ray, boneWidth);
            foreach (var hit in hits)
            {
                if (hit.collider.gameObject.tag == "Ground")
                {
                    return hit.point;
                }
            }

            return null;
        }
    }

    public bool IsOnGround()
    {
        if (GroundPoint != null)
        {
            return true;
        }

        return false;
    }

    private void OnDrawGizmos()
    {
        if (IsOnGround())
        {
            var g = Color.green;
            g.a /= 3;
            Gizmos.color = g;
        }
        else
        {
            var r = Color.red;
            r.a /= 3;
            Gizmos.color = r;
        }
        Gizmos.DrawSphere(transform.position, boneWidth);
    }
}
