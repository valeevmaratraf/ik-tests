using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class FootholdCenterCalculatorIK : MonoBehaviour
{
    private List<Fulcrum> fulcrums;
    private List<ConstraintSource> sources;
    private PositionConstraint positionConstraint;

    //private bool isInitialized;

    private void Start()
    {
        positionConstraint = GetComponent<PositionConstraint>();
        sources = new List<ConstraintSource>(positionConstraint.sourceCount);
        positionConstraint.GetSources(sources);

        fulcrums = new List<Fulcrum>(positionConstraint.sourceCount);
        for (int i = 0; i < sources.Count; i++)
        {
            if (sources[i].sourceTransform.TryGetComponent<Fulcrum>(out var fulcrum))
            {
                fulcrums.Add(fulcrum);
            }
        }
    }

    private Vector3 CalculateFoothold()
    {
        //if (!isInitialized)
        //{
        //    Initialize();
        //}

        positionConstraint.GetSources(sources);
        for (int i = 0; i < fulcrums.Count; i++)
        {
            float weight = fulcrums[i].IsOnGround() ? 1 : 0;
            sources[i] = new ConstraintSource()
            {
                sourceTransform = sources[i].sourceTransform,
                weight = weight
            };
        }

        positionConstraint.SetSources(sources);
        return transform.position;
    }

    public bool IsOnGround()
    {
        //if (!isInitialized)
        //{
        //    Initialize();
        //}

        foreach(var fulcrum in fulcrums)
        {
            if (fulcrum.IsOnGround())
            {
                return true;
            }
        }

        return false;
    }

    //private void Initialize()
    //{
    //    positionConstraint = GetComponent<PositionConstraint>();
    //    sources = new List<ConstraintSource>(positionConstraint.sourceCount);
    //    positionConstraint.GetSources(sources);

    //    fulcrums = new List<Fulcrum>(positionConstraint.sourceCount);
    //    for (int i = 0; i < sources.Count; i++)
    //    {
    //        if (sources[i].sourceTransform.TryGetComponent<Fulcrum>(out var fulcrum))
    //        {
    //            fulcrums.Add(fulcrum);
    //        }
    //    }

    //    isInitialized = true;
    //}

    private void Update()
    {
        CalculateFoothold();
    }
}
