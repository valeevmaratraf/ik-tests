using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

[RequireComponent(typeof(CharacterFootIK))]
public class FootTargetRotator : MonoBehaviour
{
    [SerializeField] private Transform footBone;
    [SerializeField] private Transform toeBone;

    [SerializeField] private Fulcrum footFulcrum;
    [SerializeField] private Fulcrum toeFulcrum;

    [SerializeField] private Transform forwardReference;
    [SerializeField] private Transform overrideFootRotationSource;

    [SerializeField] private float toGroundOffset;

    private float footLenght;

    private void Start()
    {
        transform.rotation = footBone.rotation;
        overrideFootRotationSource.LookAt(toeBone.position, Vector3.up);
        footLenght = Vector3.Distance(footBone.transform.position, toeBone.transform.position);
    }

    private void Update()
    {
        overrideFootRotationSource.position = footBone.position;

        if (footFulcrum.IsOnGround())
        {
            return;
        }

        Ray rayToGround = new Ray(footBone.transform.position, -forwardReference.up);
        var hits = Physics.RaycastAll(rayToGround, 10);

        foreach (var hit in hits)
        {
            if (hit.collider.tag == "Ground")
            {
                var toeOffsetUp = toeFulcrum.BoneWidth * rayToGround.direction * -1;
                float height = Vector3.Distance(footBone.transform.position, hit.point) - toeFulcrum.BoneWidth;

                if (height > footLenght)
                {
                    return;
                }

                var toeProjectedLenght = Mathf.Sqrt((footLenght * footLenght) - (height * height));
                var toeDestinationPoint = hit.point + toeOffsetUp + forwardReference.forward * toeProjectedLenght
                    - forwardReference.up * (toGroundOffset * height / footLenght);
                overrideFootRotationSource.LookAt(toeDestinationPoint, Vector3.up);

                break;
            }
        }
    }
}
