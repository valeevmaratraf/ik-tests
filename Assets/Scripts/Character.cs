using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public enum CharacterState
{
    Idle,
    Move
}

[RequireComponent(typeof(MovePointReceiver), typeof(CharacterMoveCompleteChecker))]
public class Character : MonoBehaviour
{
    [SerializeField] private MovePointReceiver movePointReceiver;
    private CharacterMoveCompleteChecker moveCompleteChecker;

    private CharacterState state;

    public CharacterState State
    {
        get => state;
        set
        {
            if (value == state)
            {
                return;
            }

            var oldState = value;
            state = value;
            StateChanged?.Invoke(oldState, state);
        }
    }

    public event Action<CharacterState, CharacterState> StateChanged;

    private void Awake()
    {
        moveCompleteChecker = GetComponent<CharacterMoveCompleteChecker>();
    }

    private void Start()
    {
        movePointReceiver.NewMovePointRecieved += HandleNewMoveComandRecieved;
        moveCompleteChecker.MovedToDestinationPoint += HandleMovedToDestinationPoint;
    }

    private void HandleNewMoveComandRecieved(Vector3 point)
    {
        moveCompleteChecker.DestinationPoint = point;

        State = CharacterState.Move;
    }

    private void HandleMovedToDestinationPoint()
    {
        State = CharacterState.Idle;
    }
}
